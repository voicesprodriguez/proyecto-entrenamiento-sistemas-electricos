using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class pasos_para_pinza : MonoBehaviour
{

    private Animator animador;
    


    // Start is called before the first frame update
    void Start()
    {
        animador = GetComponent<Animator>();       
    }

    // Paso 1
    public void colocarPinzas()
    {
        animador.SetBool("colocarPinzas", true);       
    }

    // Paso 2
    public void tornillos()
    {
        animador.SetBool("tornillos", true);
    }

    // Paso 3 aux
    public void abrir_molde_pinza_sub()
    {
        animador.SetBool("abrir_molde_pinza_sub", true);
    }

    // Paso 4 aux
    public void cerrar_molde_pinza_sub()
    {
        Invoke(nameof(cerrar_molde_pinza), 6.22f);
    }


    void cerrar_molde_pinza()
    {
        animador.SetBool("cerrar_pinza", true);
    }

    // Paso 11 aux
    public void Abrir_molde_pinza_fin()
    {
        animador.SetBool("finp", true);
    }


}
