using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class antorcha_pasos : MonoBehaviour
{
    private Animator animador;
    void Start()
    {
        animador = GetComponent<Animator>();
    }

    // Paso 10
    public void col_antorcha()
    {
        animador.SetBool("fuego", true);
    }
}
