using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pasos_para_molde : MonoBehaviour
{
    private Animator animador;

    void Start()
    {
        animador = GetComponent<Animator>();
    }

    // Paso 3

    public void abrir_molde()
    {
        animador.SetBool("abrir_molde", true);
    }

    // Paso 4
    public void col_cables()
    {
        animador.SetBool("col_cables", true);
    }

    // Paso 5
    public void abrir_tapa()
    {
        animador.SetBool("abrir_tapa", true);
    }

    // Paso 9 aux
    public void cerrar_tapa()
    {
        Invoke(nameof(cerrar_tapa_aux), 15.14f);
    }

    void cerrar_tapa_aux()
    {
        animador.SetBool("cerrar_tapa", true);
    }

    // Paso 11 aux
    public void abrir_tapa_fin()
    {
        animador.SetBool("fin", true);
    }

}
