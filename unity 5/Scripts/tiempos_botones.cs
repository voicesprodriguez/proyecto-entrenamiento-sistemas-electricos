using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tiempos_botones : MonoBehaviour
{
    public ParticleSystem polvo1part;
    public ParticleSystem polvo2part;
    public ParticleSystem polvo3part;
    public ParticleSystem fuego1;
    

    public ParticleSystem fuegoPart;

    public GameObject boton1;
    public GameObject boton2;
    public GameObject boton3;
    public GameObject boton4;
    public GameObject boton5;
    public GameObject boton6;
    public GameObject boton7;
    public GameObject boton8;
    public GameObject boton9;
    public GameObject boton10;
    public GameObject boton11;

    public GameObject polvorad;


    // Start is called before the first frame update
    void Start()
    {
        boton2.SetActive(false);
        boton3.SetActive(false);
        boton4.SetActive(false);        
        boton5.SetActive(false);
        boton6.SetActive(false);
        boton7.SetActive(false);
        boton8.SetActive(false);
        boton9.SetActive(false);
        boton10.SetActive(false);
        boton11.SetActive(false);

    }

    // Update is called once per frame
    public void fboton1()
    {
        boton1.SetActive(false);
        Invoke("mostrarBot2", 8.1f);
    }

    public void fboton2()
    {
        boton2.SetActive(false);
        Invoke("mostrarBot3", 3.07f);
    }

    public void fboton3()
    {
        boton3.SetActive(false);
        Invoke("mostrarBot4", 2.5f);
    }

    public void fboton4()
    {
        boton4.SetActive(false);
        Invoke("mostrarBot5", 7.74f);
    }

    public void fboton5()
    {
        boton5.SetActive(false);
        Invoke(nameof(mostrarBot6), 2.1f);
    }

    public void fboton6()
    {
        boton6.SetActive(false);
        Invoke(nameof(mostrarBot7), 6.42f);
    }

    public void fboton7()
    {        
        polvo1part.Play();        
        boton7.SetActive(false);
        Invoke(nameof(mostrarBot8), 9.67f);
    }

    public void fboton8()
    {
        polvo2part.Play();
        boton8.SetActive(false);
        Invoke(nameof(mostrarBot9), 11.92f);
    }

    public void fboton9()
    {
        polvo3part.Play();
        boton9.SetActive(false);
        Invoke(nameof(mostrarBot10), 17.18f);
    }

    public void fboton10()
    {
        fuegoPart.Play();
        boton10.SetActive(false);
        Invoke(nameof(encender_explosiones), 3.3f);
        Invoke(nameof(mostrarBot11), 5.68f);
    }

    public void fboton11()
    {
        boton11.SetActive(false);
    }


    void mostrarBot2()
    {
        boton2.SetActive(true);
    }

    void mostrarBot3()
    {
        boton3.SetActive(true);
    }

    void mostrarBot4()
    {
        boton4.SetActive(true);
    }

    void mostrarBot5()
    {
        boton5.SetActive(true);
    }

    void mostrarBot6()
    {
        boton6.SetActive(true);
    }

    void mostrarBot7()
    {
        boton7.SetActive(true);
    }

    void mostrarBot8()
    {
        boton8.SetActive(true);
    }

    void mostrarBot9()
    {
        boton9.SetActive(true);
    }

    void mostrarBot10()
    {
        boton10.SetActive(true);
    }

    void mostrarBot11()
    {
        boton11.SetActive(true);
    }


    void encender_explosiones()
    {
        fuego1.Play();
        polvorad.SetActive(false);
    }
}
