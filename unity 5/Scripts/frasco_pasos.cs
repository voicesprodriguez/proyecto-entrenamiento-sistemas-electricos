using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class frasco_pasos : MonoBehaviour
{
    private Animator animador;
    void Start()
    {
        animador = GetComponent<Animator>();
    }


    // Paso 6
    public void col_disco()
    {
        animador.SetBool("ponerDisco", true);
    }

    // Paso 7
    public void col_pol1()
    {
        animador.SetBool("poner_polvo1", true);
    }

    // Paso 8
    public void col_pol2()
    {
        animador.SetBool("poner_polvo2", true);
    }

    // Paso 9
    public void col_pol3()
    {
        animador.SetBool("poner_polvo3", true);
    }

}
